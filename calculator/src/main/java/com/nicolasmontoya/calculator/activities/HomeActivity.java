package com.nicolasmontoya.calculator.activities;

import android.os.Bundle;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nicolasmontoya.calculator.R;

/**
 * Created by nicolas on 18/02/18.
 */

public class HomeActivity extends AppCompatActivity {

    final static String TAG = "HomeActivity";
    private double ans;
    private int position;
    private String[] log;
    private String number;
    private double[] numbers;
    private int[] operators;
    private String screen;
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.tvScreen);
        screen = "";
        numbers = new double[20];
        operators = new int[10];
        position = 0;
        ans = 0;
    }

    public void btnClick(View view){
        Log.d(TAG, "VIEWCLICK: "+view.getId());
        switch (view.getId()){
            case R.id.btn0:
                    if(screen == null || screen.isEmpty())
                    {
                        number = "0";
                        screen = "0";
                    }else{
                        number = number + '0';
                        screen = screen + '0';
                    }

                break;
            case R.id.btn1:
                Log.d(TAG, "UNO PUSH: ");
                if(screen==null || screen.isEmpty())
                {
                    number = "1";
                    screen = "1";
                }else{
                    number = number + '1';
                    screen = screen +'1';
                }
                break;
            case R.id.btn2:
                if(screen==null || screen.isEmpty())
                {
                    number = "2";
                    screen = "2";
                }else{
                    number = number + '2';
                    screen = screen +'2';
                }
                break;
            case R.id.btn3:
                if(screen==null || screen.isEmpty())
                {
                    number = "3";
                    screen = "3";
                }else{
                    number = number + '3';
                    screen = screen +'3';
                }
                break;
            case R.id.btn4:
                if(screen==null || screen.isEmpty())
                {
                    number = "4";
                    screen = "4";
                }else{
                    number = number + '4';
                    screen = screen +'4';
                }
                break;
            case R.id.btn5:
                if(screen==null || screen.isEmpty())
                {
                    number = "5";
                    screen = "5";
                }else{
                    number = number + '5';
                    screen = screen +'5';
                }
                break;
            case R.id.btn6:
                if(screen==null || screen.isEmpty())
                {
                    number = "6";
                    screen = "6";
                }else{
                    number = number + '6';
                    screen = screen +'6';
                }
                break;
            case R.id.btn7:
                if(screen==null || screen.isEmpty())
                {
                    number = "7";
                    screen = "7";
                }else{
                    number = number + '7';
                    screen = screen +'7';
                }
                break;
            case R.id.btn8:
                if(screen==null || screen.isEmpty())
                {
                    number = "8";
                    screen = "8";
                }else{
                    number = number + '8';
                    screen = screen +'8';
                }
                break;
            case R.id.btn9:
                if(screen==null || screen.isEmpty())
                {
                    number = "9";
                    screen = "9";
                }else{
                    number = number + '9';
                    screen = screen +'9';
                }
                break;
            case R.id.btnP:
                if(screen==null || screen.isEmpty())
                {

                }else{
                    int flag = 0;
                    for(int a=0; a < number.length() ; a++){
                        if(number.contains(".")){
                            flag = 1;
                        }
                    }
                    if(flag == 0){
                        number = number + '.';
                        screen = screen +'.';
                    }
                }
                break;
            case R.id.btnSub:
                numbers[position] = Double.parseDouble(number);
                operators[position] = 2;
                number = "";
                position++;
                screen = screen + '-';
                break;
            case R.id.btnSum:

                numbers[position] = Double.parseDouble(number);
                Log.d(TAG, "btnClick: "+numbers[position]);
                operators[position] = 1;
                number = "";
                position++;
                screen = screen + '+';
                break;
            case R.id.btnMul:
                numbers[position] = Double.parseDouble(number);
                operators[position] = 3;
                number = "";
                position++;
                screen = screen + '*';
                break;
            case R.id.btnDiv:
                numbers[position] = Double.parseDouble(number);
                operators[position] = 4;
                number = "";
                position++;
                screen = screen + '/';
                break;
            case R.id.btnC:
                ans = 0;
                position = 0;
                number = "";
                numbers = new double[10];
                operators = new int[10];
                screen = "";
                break;
            case R.id.btnDel:
                Log.d(TAG, "btnClick: "+position);
                if(number == "" && position > 0){
                    number = String.valueOf(numbers[position-1]);
                    int j = 0;
                    int[] tmp = new int[10];
                    for (int i = 0; i < operators.length-1; i++) {
                            tmp[i] = operators[i];
                    }
                    operators = tmp;
                    position--;
                    screen = screen.substring(0, screen.length()-1);
                }else{
                    if (number.length() == 1){
                        number= "";
                    }else {
                        number = number.substring(0,number.length()-1);
                    }

                    screen = screen.substring(0, screen.length()-1);
                }
                break;
            case R.id.btnEqual:
                if (ans != 0){
                    numbers[0] = ans;
                }
                numbers[position] = Double.parseDouble(number);
                if(!number.equals("")){
                    int j = 0;
                    for(int k=0; k < operators.length;k++){
                        if(operators[k] == 1){
                            if(k==0){
                                ans = numbers[j]+numbers[j+1];
                                j+=2;
                            }else{
                                ans = ans + numbers[j];
                                j++;
                            }
                        }
                        else if(operators[k] == 2){
                            if(k==0){
                                ans =  numbers[j]-numbers[j+1];
                                j+=2;
                            }else{
                                ans = ans - numbers[j];
                                j++;
                            }
                        }
                        else if(operators[k] == 3){
                            if(k==0){
                                ans =  numbers[j]*numbers[j+1];
                                j+=2;
                            }else{
                                ans = ans * numbers[j];
                                j++;
                            }
                        }
                        else if(operators[k] == 4){
                            if(k==0){
                                ans =  numbers[j]/numbers[j+1];
                                j+=2;
                            }else{
                                ans = ans / numbers[j];
                                j++;
                            }
                        }

                    }
                    j=0;
                }
                screen = String.valueOf(ans);
                position = 0;
                break;
        }
        Log.d(TAG, "btnClick: "+screen);
        textView.setText(screen);
    }
}
