package com.nicolasmontoya.resistorcalc;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nicolasmontoya.resistorcalc.Adapters.SpinnerAdapter;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Spinner spinner;
    Spinner spinner2;
    Spinner spinner3;
    Spinner spinner4;
    TextView textView;
    TextView txs;

    Integer[] lineOne = {2,3,4,5,6,7,8,9,10};
    Integer[] lineTwo = {1,2,3,4,5,6,7,8,9,10};
    Integer[] lineThree = {1,2,3,4,5,6,7,8,9,10,11,12};
    Integer[] lineFour = {1,2,3,4};

    Integer[] counter = {1,1,1};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spinnerOne);
        spinner2 = findViewById(R.id.spinnerTwo);
        spinner3 = findViewById(R.id.spinnerThree);
        spinner4 = findViewById(R.id.spinnerFour);
        textView = findViewById(R.id.texts);
        txs = findViewById(R.id.txs);

        SpinnerAdapter spinnerAdapterOne = new SpinnerAdapter(this,R.layout.spinner_list,lineOne);
        SpinnerAdapter spinnerAdapterTwo = new SpinnerAdapter(this,R.layout.spinner_list,lineTwo);
        SpinnerAdapter spinnerAdapterThree = new SpinnerAdapter(this,R.layout.spinner_list,lineThree);
        SpinnerAdapter spinnerAdapterFour = new SpinnerAdapter(this,R.layout.spinner_list,lineFour);
        spinner.setAdapter(spinnerAdapterOne);
        spinner2.setAdapter(spinnerAdapterTwo);
        spinner3.setAdapter(spinnerAdapterThree);
        spinner4.setAdapter(spinnerAdapterFour);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int value =(int) adapterView.getItemAtPosition(i);
                double data = switchValues(1,value);
                resumeResponse(data);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int value =(int) adapterView.getItemAtPosition(i);
                double data = switchValues(2,value);
                resumeResponse(data);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int value =(int) adapterView.getItemAtPosition(i);
                double data = switchValues(3,value);
                resumeResponse(data);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int value =(int) adapterView.getItemAtPosition(i);
                if(value == 1){
                    txs.setText("±1%");
                }else if(value == 2){
                    txs.setText("±2%");
                }else if( value == 3){
                    txs.setText("±5%");
                }else{
                    txs.setText("±10%");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




    }
    public double switchValues(int i,int value){
        double dataReturn ;
        counter[i-1] = value-1;
        switch (counter[2]){
            case 10:
                dataReturn= (counter[0]*10+counter[1])/10.00;
                break;
            case 11:
                Log.d("case7", "counter0: "+counter[0]);
                Log.d("case7", "counter1: "+counter[1]);
                dataReturn= ((counter[0]*10+counter[1])/100.00);
                Log.d("case7", "resumeResponse1: "+dataReturn);
                break;
            default:
                dataReturn= (counter[0]*10+counter[1])*Math.pow(10,counter[2]);
        }
        return dataReturn;
    }
    @SuppressLint("SetTextI18n")
    public void resumeResponse(double value){
        double transform;

        if(value >= .1 && value < 1000){
            textView.setText(String.format(Locale.US,"%.2f",value)+" Ω");
        }else if(value >= 1000 && value < 1000000){
            transform = value / 1000;
            textView.setText(String.format(Locale.US,"%.2f",transform)+" KΩ");
        }else if(value >= 1000000 && value < 1000000000){
            transform = value / 1000000;
            textView.setText(String.format(Locale.US,"%.2f",transform)+" MΩ");
        }else{
            transform = value / 1000000000;
            textView.setText(String.format(Locale.US,"%.2f",transform)+" GΩ");
        }
    }
}
