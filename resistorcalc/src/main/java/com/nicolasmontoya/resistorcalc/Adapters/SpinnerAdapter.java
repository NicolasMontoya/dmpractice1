package com.nicolasmontoya.resistorcalc.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasmontoya.resistorcalc.R;


/**
 * Adaptador de la clase ArrayAdapter para controlar spinner. Datos importantes !!!. La función getDropDownView
 * tiene como obejtivo controlar la vista del spinner luego de tocar el elemento de despliegue y la función view
 * se encarga de la vista del elemento en si mismo antes de ser tocada. NO NECESARIAMENTE DEBEN SER IGUALES.
 *
 * @author Nicolas Montoya - nicolas96.un@udea.edu.co
 * @version 0.0.1
 */

public class SpinnerAdapter extends ArrayAdapter<Integer> {

    private Context mContext;
    private Integer[] colors;

    public SpinnerAdapter(@NonNull Context context, int resource , Integer[] colors) {
        super(context, resource,colors);
        this.mContext = context;
        this.colors = colors;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.spinner_drop, parent, false);
        ImageView iv = (ImageView) view.findViewById(R.id.myimg2);
        if(colors.length > 4 && colors.length < 11)
        {
            switch (colors[position]){
                case 1:
                    iv.setBackgroundResource(R.color.black);
                    break;
                case 2:
                    iv.setBackgroundResource(R.color.brown);
                    break;
                case 3:
                    iv.setBackgroundResource(R.color.red);
                    break;
                case 4:
                    iv.setBackgroundResource(R.color.orange);
                    break;
                case 5:
                    iv.setBackgroundResource(R.color.yellow);
                    break;
                case 6:
                    iv.setBackgroundResource(R.color.green);
                    break;
                case 7:
                    iv.setBackgroundResource(R.color.blue);
                    break;
                case 8:
                    iv.setBackgroundResource(R.color.purple);
                    break;
                case 9:
                    iv.setBackgroundResource(R.color.gray);
                    break;
                case 10:
                    iv.setBackgroundResource(R.color.white);
                    break;
            }
        }else if(colors.length == 12){
            switch (colors[position]){
                case 1:
                    iv.setBackgroundResource(R.color.black);
                    break;
                case 2:
                    iv.setBackgroundResource(R.color.brown);
                    break;
                case 3:
                    iv.setBackgroundResource(R.color.red);
                    break;
                case 4:
                    iv.setBackgroundResource(R.color.orange);
                    break;
                case 5:
                    iv.setBackgroundResource(R.color.yellow);
                    break;
                case 6:
                    iv.setBackgroundResource(R.color.green);
                    break;
                case 7:
                    iv.setBackgroundResource(R.color.blue);
                    break;
                case 8:
                    iv.setBackgroundResource(R.color.purple);
                    break;
                case 9:
                    iv.setBackgroundResource(R.color.gray);
                    break;
                case 10:
                    iv.setBackgroundResource(R.color.white);
                    break;
                case 11:
                    iv.setBackgroundResource(R.color.gold);
                    break;
                case 12:
                    iv.setBackgroundResource(R.color.silver);
                    break;
            }
        }else{
            switch (colors[position]){
                case 1:
                    iv.setBackgroundResource(R.color.brown);
                    break;
                case 2:
                    iv.setBackgroundResource(R.color.red);
                    break;
                case 3:
                    iv.setBackgroundResource(R.color.silver);
                    break;
                case 4:
                    iv.setBackgroundResource(R.color.darkgoldenrod);
                    break;
            }
        }

        return view;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.spinner_list, parent, false);
        ImageView iv = (ImageView) view.findViewById(R.id.myimg);
        if(colors.length > 4 && colors.length < 11)
        {
            switch (colors[position]){
                case 1:
                    iv.setBackgroundResource(R.color.black);
                    break;
                case 2:
                    iv.setBackgroundResource(R.color.brown);
                    break;
                case 3:
                    iv.setBackgroundResource(R.color.red);
                    break;
                case 4:
                    iv.setBackgroundResource(R.color.orange);
                    break;
                case 5:
                    iv.setBackgroundResource(R.color.yellow);
                    break;
                case 6:
                    iv.setBackgroundResource(R.color.green);
                    break;
                case 7:
                    iv.setBackgroundResource(R.color.blue);
                    break;
                case 8:
                    iv.setBackgroundResource(R.color.purple);
                    break;
                case 9:
                    iv.setBackgroundResource(R.color.gray);
                    break;
                case 10:
                    iv.setBackgroundResource(R.color.white);
                    break;
            }
        }else if(colors.length == 12){
            switch (colors[position]){
                case 1:
                    iv.setBackgroundResource(R.color.black);
                    break;
                case 2:
                    iv.setBackgroundResource(R.color.brown);
                    break;
                case 3:
                    iv.setBackgroundResource(R.color.red);
                    break;
                case 4:
                    iv.setBackgroundResource(R.color.orange);
                    break;
                case 5:
                    iv.setBackgroundResource(R.color.yellow);
                    break;
                case 6:
                    iv.setBackgroundResource(R.color.green);
                    break;
                case 7:
                    iv.setBackgroundResource(R.color.blue);
                    break;
                case 8:
                    iv.setBackgroundResource(R.color.purple);
                    break;
                case 9:
                    iv.setBackgroundResource(R.color.gray);
                    break;
                case 10:
                    iv.setBackgroundResource(R.color.white);
                    break;
                case 11:
                    iv.setBackgroundResource(R.color.gold);
                    break;
                case 12:
                    iv.setBackgroundResource(R.color.silver);
                    break;
            }
        }else{
            switch (colors[position]){
                case 1:
                    iv.setBackgroundResource(R.color.brown);
                    break;
                case 2:
                    iv.setBackgroundResource(R.color.red);
                    break;
                case 3:
                    iv.setBackgroundResource(R.color.silver);
                    break;
                case 4:
                    iv.setBackgroundResource(R.color.darkgoldenrod);
                    break;
            }
        }

        return view;
    }
}

