package com.nicolasmontoya.figures;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nicolasmontoya.figures.models.Circle;
import com.nicolasmontoya.figures.models.Cube;
import com.nicolasmontoya.figures.models.Rectangle;
import com.nicolasmontoya.figures.models.Square;
import com.nicolasmontoya.figures.models.Triangle;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup radioGroup;
    private EditText editText1;
    private EditText editText2;
    private TextView tvPerimeter;
    private TextView tvArea;
    private TextView tvVolume;
    private LinearLayout linearLayout;
    private Button btnOk;
    private int figure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.sideOne);
        editText2 = findViewById(R.id.sideTwo);
        tvPerimeter = findViewById(R.id.tvPerimeter);
        tvArea = findViewById(R.id.tvArea);
        tvVolume = findViewById(R.id.tvVolume);
        radioGroup = findViewById(R.id.rbSelectFigure);
        linearLayout = findViewById(R.id.response);
        btnOk = findViewById(R.id.btnOk);

        btnOk.setOnClickListener(this);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                btnOk.setEnabled(true);
                switch (checkedId){
                    case R.id.square:
                        editText1.setVisibility(View.VISIBLE);
                        editText1.setText("");
                        editText1.setHint("Radius");
                        editText2.setVisibility(View.GONE);
                        editText2.setText("");
                        figure = R.id.square;
                        linearLayout.setVisibility(View.GONE);
                        break;
                    case R.id.rectangle:
                        editText1.setVisibility(View.VISIBLE);
                        editText1.setText("");
                        editText1.setHint("Width");
                        editText2.setVisibility(View.VISIBLE);
                        editText2.setText("");
                        editText2.setHint("Height");
                        figure = R.id.rectangle;
                        linearLayout.setVisibility(View.GONE);
                        break;
                    case R.id.triangle:
                        editText1.setVisibility(View.VISIBLE);
                        editText1.setText("");
                        editText1.setHint("Base");
                        editText2.setVisibility(View.VISIBLE);
                        editText2.setText("");
                        editText2.setHint("Height");
                        figure = R.id.triangle;
                        linearLayout.setVisibility(View.GONE);
                        break;
                    case R.id.cube:
                        editText1.setVisibility(View.VISIBLE);
                        editText1.setText("");
                        editText1.setHint("Side");
                        editText2.setVisibility(View.GONE);
                        editText2.setText("");
                        figure = R.id.cube;
                        linearLayout.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        if(figure == R.id.cube || figure == R.id.square){
            if(editText1.getText() != null && !editText1.getText().toString().isEmpty()){
                switch (figure){
                    case R.id.square:
                        Circle square = new Circle(Double.parseDouble(editText1.getText().toString()));
                        tvPerimeter.setText("Perimeter : "+String.format("%.1f",square.getPerimeter()));
                        tvArea.setText("Area : "+String.format("%.1f",square.getArea()));
                        tvVolume.setText("Volume : NA");
                        linearLayout.setVisibility(View.VISIBLE);
                        break;
                    case R.id.cube:
                        Cube cube = new Cube(Double.parseDouble(editText1.getText().toString()));
                        tvPerimeter.setText("Perimeter : "+String.valueOf(cube.getPerimeter()));
                        tvArea.setText("Area : "+String.valueOf(cube.getArea()));
                        tvVolume.setText("Volume : "+String.valueOf(cube.getVolume()));
                        linearLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }else{
            if(editText1.getText() != null && !editText1.getText().toString().isEmpty() && editText2.getText() != null && !editText2.getText().toString().isEmpty()){
                switch (figure){
                    case R.id.rectangle:
                        Rectangle rectangle = new Rectangle(Double.parseDouble(editText1.getText().toString()),Double.parseDouble(editText2.getText().toString()));
                        tvPerimeter.setText("Perimeter : "+String.valueOf(rectangle.getPerimeter()));
                        tvArea.setText("Area : "+String.valueOf(rectangle.getArea()));
                        tvVolume.setText("Volume : NA");
                        linearLayout.setVisibility(View.VISIBLE);
                        break;
                    case R.id.triangle:
                        Triangle triangle = new Triangle(Double.parseDouble(editText1.getText().toString()),Double.parseDouble(editText2.getText().toString()));
                        tvPerimeter.setText("Perimeter : "+String.format("%.1f",triangle.getPerimeter()));
                        tvArea.setText("Area : "+String.format("%.1f",triangle.getArea()));
                        tvVolume.setText("Volume : NA");
                        linearLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

    }
}
