package com.nicolasmontoya.figures.models;

/**
 * Created by nicolas on 18/02/18.
 */

public class Square {
    private double side;
    private double perimeter;
    private double area;

    public Square(double side) {
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double getPerimeter() {
        perimeter = 4 * side;
        return perimeter;
    }


    public double getArea() {
        area = side * side;
        return area;
    }

}
