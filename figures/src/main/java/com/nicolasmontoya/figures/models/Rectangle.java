package com.nicolasmontoya.figures.models;

/**
 * Created by nicolas on 18/02/18.
 */

public class Rectangle {

    private double height;
    private double width;
    private double perimeter;
    private double area;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getPerimeter() {
        perimeter = 2*height+2*width;
        return perimeter;
    }


    public double getArea() {
        area = height*width;
        return area;
    }

}
