package com.nicolasmontoya.figures.models;

/**
 * Created by nicolas on 18/02/18.
 */

public class Circle {
    private double radius;
    private double perimeter;
    private double area;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getPerimeter() {
        perimeter = 2 * Math.PI * radius;
        return perimeter;
    }


    public double getArea() {
        area = Math.PI * radius * radius;
        return area;
    }

}
