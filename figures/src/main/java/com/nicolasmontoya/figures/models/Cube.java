package com.nicolasmontoya.figures.models;

/**
 * Created by nicolas on 18/02/18.
 */

public class Cube extends Square{
    private double volume;

    public Cube(double side) {
        super(side);
    }

    public double getVolume() {
        volume = this.getArea()*getSide();
        return volume;
    }
    @Override
    public double getArea() {
        return 6*super.getArea();
    }

}
