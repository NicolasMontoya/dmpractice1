package com.nicolasmontoya.figures.models;

/**
 * Created by nicolas on 18/02/18.
 */

public class Triangle {
    private double base;
    private double height;
    private double perimeter;
    private double area;

    public Triangle(double base, double height) {
        this.height = height;
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return base;
    }

    public void setWidth(double width) {
        this.base = width;
    }

    public double getPerimeter() {
        perimeter = base + 2*Math.sqrt((base/2)*(base/2)+height*height);
        return perimeter;
    }


    public double getArea() {
        area = (height*base)/2;
        return area;
    }
}
