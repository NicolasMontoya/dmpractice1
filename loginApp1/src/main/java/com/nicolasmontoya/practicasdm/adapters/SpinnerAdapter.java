package com.nicolasmontoya.practicasdm.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasmontoya.practicasdm.R;


/**
 * Adaptador de la clase ArrayAdapter para controlar spinner. Datos importantes !!!. La función getDropDownView
 * tiene como obejtivo controlar la vista del spinner luego de tocar el elemento de despliegue y la función view
 * se encarga de la vista del elemento en si mismo antes de ser tocada. NO NECESARIAMENTE DEBEN SER IGUALES.
 *
 * @author Nicolas Montoya - nicolas96.un@udea.edu.co
 * @version 0.0.1
 */

public class SpinnerAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private String[] places;

    public SpinnerAdapter(@NonNull Context context, int resource , String[] places) {
        super(context, resource,places);
        this.mContext = context;
        this.places = places;
    }

    @Override
    public boolean isEnabled(int position) {
        if(position == 0){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.spinner_list, parent, false);
        TextView tx = (TextView) view.findViewById(R.id.text);
        if(position == 0){
            tx.setTextColor(Color.GRAY);
        }else{
            tx.setTextColor(Color.BLACK);
        }
        tx.setText(places[position]);

        return view;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.spinner_view, parent, false);
        TextView tv = view.findViewById(R.id.tvCity);
        ImageView img = view.findViewById(R.id.ivCity);
        if(position == 0){
            tv.setTextColor(Color.GRAY);
        }else{
            tv.setTextColor(Color.BLACK);
            img.setImageResource(R.drawable.colombia_flag);
        }
        tv.setText(places[position]);

        return view;
    }
}
