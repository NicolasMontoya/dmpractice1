package com.nicolasmontoya.practicasdm.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by nicolas on 16/02/18.
 */

public class User {
    private String username;
    private String password;
    private String birthday;
    private String genere;
    private String city;
    private  String email;
    private String phone;
    private  ArrayList<String> hobbies;


    public User(String username, String password, String birthday, String genere, String city, String email) {
        this.username = username;
        this.password = password;
        this.birthday = birthday;
        this.genere = genere;
        this.city = city;
        this.email = email;
    }

    public User(){}

    public ArrayList<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(ArrayList<String> hobbies) {
        this.hobbies = hobbies;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
