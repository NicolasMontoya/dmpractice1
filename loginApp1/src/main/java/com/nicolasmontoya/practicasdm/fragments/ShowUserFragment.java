package com.nicolasmontoya.practicasdm.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.nicolasmontoya.practicasdm.R;
import com.nicolasmontoya.practicasdm.models.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by nicolas on 18/02/18.
 */

public class ShowUserFragment extends Fragment {

    private User user;
    private TextView tvUsername;
    private TextView tvPassword;
    private TextView tvEmail;
    private TextView tvPhone;
    private TextView tvDate;
    private TextView tvGender;
    private TextView tvCity;
    private ListView mainListView;
    private ArrayAdapter<String> listAdapter ;

    public static ShowUserFragment newInstance(User user){
        ShowUserFragment showUserFragment = new ShowUserFragment();
        showUserFragment.setUser(user);
        return showUserFragment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        tvUsername = view.findViewById(R.id.tvUsername);
        tvPassword = view.findViewById(R.id.tvPassword);
        tvEmail = view.findViewById(R.id.tvEmail);
        tvDate = view.findViewById(R.id.tvDate);
        tvCity = view.findViewById(R.id.tvCities);
        tvGender = view.findViewById(R.id.tvGender);
        tvPhone = view.findViewById(R.id.tvPhone);
        mainListView = view.findViewById(R.id.mainListView);

        if(user.getHobbies() != null){
            String miArray[] = new String[user.getHobbies().size()];
            miArray = user.getHobbies().toArray(miArray);
            listAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_item, miArray);
            mainListView.setAdapter(listAdapter);
        }

        tvUsername.setText(tvUsername.getText()+user.getUsername());
        tvPassword.setText(tvPassword.getText()+user.getPassword());
        tvEmail.setText(tvEmail.getText()+user.getEmail());
        tvGender.setText(tvGender.getText()+user.getGenere());
        tvDate.setText(tvDate.getText()+user.getBirthday());
        tvPhone.setText(tvPhone.getText()+user.getPhone());
        tvCity.setText(tvCity.getText()+user.getCity());
    }
}
