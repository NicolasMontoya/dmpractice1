package com.nicolasmontoya.practicasdm.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.Calendar;

/**
 * Clase extendida del DialogFrament para la creación de datepicker. Mendiante una funcion instanciamos
 * un objeto de DatePicker de modo que sea posible generar un DatePickerDialog.OnDateSetListener desde
 * la actividad o fragmento principal donde se llame.
 *
 * @author Nicolas Montoya
 * @see DialogFragment
 * @see DatePickerDialog.OnDateSetListener
 *
 */

public class DatePickerFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;

    // Instancia del fragmento para ser llamada en la actividad o fragmento principal.

    public static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener listener){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setListener(listener);
        return fragment;
    }

    // Setter del listener
    public void setListener(DatePickerDialog.OnDateSetListener listener){
        this.listener = listener;
    }

    // Generación del objeto DatePickerDialog
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(),this.listener,year,month,day);
    }

}
