package com.nicolasmontoya.practicasdm.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nicolasmontoya.practicasdm.R;
import com.nicolasmontoya.practicasdm.activities.SignUp.SignUpActivity;
import com.nicolasmontoya.practicasdm.adapters.SpinnerAdapter;
import com.nicolasmontoya.practicasdm.helpers.TextValidator;
import com.nicolasmontoya.practicasdm.models.User;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nicolas on 18/02/18.
 */

public class SignUpFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "SignUpFragment";
    Spinner spinner;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etRepeatPassword;
    private EditText etEmail;
    private EditText etPhone;
    private CheckBox cb_skate;
    private CheckBox cb_soccer;
    private CheckBox cb_paint;
    private CheckBox cb_sing;
    private User user;
    private EditText etDate;

    ArrayList<String> texts = new ArrayList<>();
    Button btnSubmit;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        user = new User();

        RadioGroup radioGroup = view.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                onRadioButtonClicked(checkedId);
            }
        });

        // Spinner logic
        spinner = view.findViewById(R.id.spinner);

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(),R.layout.spinner_list,this.getResources().getStringArray(R.array.cities));
        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                user.setCity((String) adapterView.getItemAtPosition(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Edit Text
        etUsername = view.findViewById(R.id.etUsername);
        etPassword = view.findViewById(R.id.etPassword);
        etRepeatPassword = view.findViewById(R.id.etRepeatPassword);
        etDate = view.findViewById(R.id.etDate);
        etEmail = view.findViewById(R.id.etEmail);
        etPhone = view.findViewById(R.id.etPhone);

        // Checkboxes
        cb_skate = view.findViewById(R.id.cb_skate);
        cb_soccer = view.findViewById(R.id.cb_soccer);
        cb_paint = view.findViewById(R.id.cb_paint);
        cb_sing = view.findViewById(R.id.cb_sing);


        cb_skate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox) view).isChecked();
                if(checked){
                    texts.add((String)((CheckBox) view).getText());
                    user.setHobbies(texts);

                }else{
                    texts.remove((String)((CheckBox) view).getText());
                    user.setHobbies(texts);
                }
                Log.d(TAG, "onClick: "+user.getHobbies());
            }
        });

        cb_soccer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox) view).isChecked();
                if(checked){
                    texts.add((String)((CheckBox) view).getText());
                    user.setHobbies(texts);

                }else{
                    texts.remove((String)((CheckBox) view).getText());
                    user.setHobbies(texts);
                }
                Log.d(TAG, "onClick: "+user.getHobbies());
            }
        });

        cb_paint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox) view).isChecked();
                if(checked){
                    texts.add((String)((CheckBox) view).getText());
                    user.setHobbies(texts);

                }else{
                    texts.remove((String)((CheckBox) view).getText());
                    user.setHobbies(texts);
                }
                Log.d(TAG, "onClick: "+user.getHobbies());
            }
        });

        cb_sing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox) view).isChecked();
                if(checked){
                    texts.add((String)((CheckBox) view).getText());
                    user.setHobbies(texts);

                }else{
                    texts.remove((String)((CheckBox) view).getText());
                    user.setHobbies(texts);
                }
                Log.d(TAG, "onClick: "+user.getHobbies());
            }
        });

        etUsername.addTextChangedListener(new TextValidator(etUsername) {
            @Override
            public void validate(TextView textView, String text) {
                if (etUsername.getText() == null || etUsername.getText().toString().isEmpty()){
                    textView.setError("Username cannot be empty");
                }else{
                    textView.setError(null);
                }
            }
        });

        etPassword.addTextChangedListener(new TextValidator(etPassword) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.getText() == null || textView.getText().toString().isEmpty()){
                    textView.setError("Password cannot be empty");
                }else{
                    textView.setError(null);
                }
            }
        });

        etRepeatPassword.addTextChangedListener(new TextValidator(etRepeatPassword) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.getText() == null || textView.getText().toString().isEmpty()){
                    textView.setError("Password cannot be empty");
                }else if(!text.equals(etPassword.getText().toString())){
                    textView.setError("Password doesn't match");
                }else{
                    textView.setError(null);
                }
            }
        });

        etEmail.addTextChangedListener(new TextValidator(etEmail) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.getText() == null || textView.getText().toString().isEmpty()){
                    textView.setError("Email cannot be empty");
                }else{
                    textView.setError(null);
                }
            }
        });

        etDate.addTextChangedListener(new TextValidator(etDate) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.getText() == null || textView.getText().toString().isEmpty()){
                    textView.setError("Date cannot be empty");
                }else{
                    textView.setError(null);
                }
            }
        });

        etPhone.addTextChangedListener(new TextValidator(etPhone) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.getText() == null || textView.getText().toString().isEmpty()){
                    textView.setError("Phone cannot be empty");
                }else{
                    textView.setError(null);
                }
            }
        });


        etDate.setOnClickListener(this);

        // Submit button
        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signup,container,false);
    }
    @Override
    public void onClick(View view){
        int id = view.getId();
        switch (id){
            case R.id.etDate:
                this.showDatePicker();
                break;
            case R.id.btnSubmit:
                boolean isValid = this.validateForm();
                if(isValid){
                    Fragment fragment = ShowUserFragment.newInstance(user);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerLayout,fragment);
                    fragmentTransaction.commit();
                }
                break;
        }

    }
    private void showDatePicker(){
        DatePickerFragment datepicker = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                String data = Integer.toString(day) +"/"+ Integer.toString(month+1) +"/"+ Integer.toString(year);
                etDate.setText(data);

            }
        });
        datepicker.show(getFragmentManager(),"Date Picker");
    }

    private boolean validateForm(){

        Toast toast = Toast.makeText(getActivity(),"",Toast.LENGTH_SHORT);
        if(etUsername.getText().toString().isEmpty()){
            etUsername.setError("Username cannot be empty");
            return false;
        }else if(etPassword.getText().toString().isEmpty()){
            etPassword.setError("Password cannot be empty");
            return false;
        }else if(etRepeatPassword.getText().toString().isEmpty()){
            etRepeatPassword.setError("Repeat password cannot be empty");
            return false;
        }else if(!isValidPassword(etPassword.getText().toString().trim())){
            etPassword.setError("Enter a valid password.");
            return false;
        }else if(etEmail.getText().toString().isEmpty()){
            etEmail.setError("Email cannot be empty");
            return false;
        }else if(!isValidEmail(etEmail.getText().toString().trim())){
            etEmail.setError("Enter a valid email");
            return false;
        }else if(etPhone.getText().toString().isEmpty()){
            etPhone.setError("Phone cannot be empty");
            return false;
        }else if(etDate.getText().toString().isEmpty()){
            etDate.setError("Date cannot be empty");
            return false;
        }else if(user.getCity() == null){
            toast.setText("Enter a city");
            toast.show();
            return false;
        }else if(!cb_sing.isChecked() && !cb_soccer.isChecked() && !cb_paint.isChecked() && !cb_skate.isChecked()){
            toast.setText("Enter at least one hobby");
            toast.show();
            return false;
        }else if(user.getGenere() == null){
            toast.setText("Enter a gender");
            toast.show();
            return false;
        }

        else if(!etPassword.getText().toString().equals(etRepeatPassword.getText().toString())){
            etPassword.setError("Password doesn't match.");
            return false;
        }
        else{
            user.setUsername(etUsername.getText().toString());
            user.setPassword(etPassword.getText().toString());
            user.setEmail(etEmail.getText().toString());
            user.setBirthday(etDate.getText().toString());
            user.setPhone(etPhone.getText().toString());
            return true;
        }

    }

    public boolean isValidPassword(final String password){
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$";
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
    public boolean isValidEmail(final String email){
        final String PASSWORD_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+";
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(email);
        return  matcher.matches();
    }

    public void onRadioButtonClicked(int checked) {
        // Is the button now checked?

        // Check which radio button was clicked
        switch(checked) {
            case R.id.rbMale:
                user.setGenere("Male");
                break;
            case R.id.rbFemale:
                user.setGenere("Female");
                break;
        }
    }
}

