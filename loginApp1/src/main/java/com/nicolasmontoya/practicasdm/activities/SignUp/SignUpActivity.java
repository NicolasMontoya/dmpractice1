package com.nicolasmontoya.practicasdm.activities.SignUp;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.nicolasmontoya.practicasdm.R;
import com.nicolasmontoya.practicasdm.adapters.SpinnerAdapter;
import com.nicolasmontoya.practicasdm.fragments.DatePickerFragment;
import com.nicolasmontoya.practicasdm.fragments.SignUpFragment;
import com.nicolasmontoya.practicasdm.models.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nicolas on 14/02/18.
 */

public class SignUpActivity extends AppCompatActivity  {

    private static final String TAG = "SignUp";





    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Fragment fragment = new SignUpFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.add(R.id.containerLayout, fragment);
        transaction.commit();

    }




}
