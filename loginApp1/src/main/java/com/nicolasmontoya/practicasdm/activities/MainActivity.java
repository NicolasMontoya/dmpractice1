package com.nicolasmontoya.practicasdm.activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.nicolasmontoya.practicasdm.R;
import com.nicolasmontoya.practicasdm.activities.SignUp.SignUpActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MyActivity";
    private String data;

    TextInputLayout eUserLayout, ePassLayout;
    TextInputEditText eUsername, ePassword;
    CheckBox meet,other;
    Button button;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        eUsername = findViewById(R.id.etUsername);
        ePassword = findViewById(R.id.etPassword);
        button = findViewById(R.id.bRegister);
        eUserLayout = findViewById(R.id.etUsernameLayout);
        ePassLayout = findViewById(R.id.etPasswordLayout);


    }
    public void clickOnButton(View view){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }



}
