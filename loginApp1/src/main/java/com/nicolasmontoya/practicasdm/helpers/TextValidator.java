package com.nicolasmontoya.practicasdm.helpers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by nicolas on 23/02/18.
 */

public abstract class TextValidator implements TextWatcher {

    TextView textView;

    public abstract void validate(TextView textView, String text);

    public TextValidator(TextView textView) {
        this.textView = textView;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String text = textView.getText().toString();
        validate(textView,text);
    }
}
